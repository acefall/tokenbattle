%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.1 (25/3/14)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Linux and Unix Users Group at Virginia Tech Wiki 
% (https://vtluug.org/wiki/Example_LaTeX_chem_lab_report)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\usepackage{graphicx} % Required for the inclusion of images
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements
\usepackage{algorithm, caption} % Required for algorithms
\usepackage[noend]{algpseudocode} % Required for Pseudo Code

\usepackage{pgfplots}
\usepackage{filecontents}
%\usepackage{subcaption}
\usepackage{subfig}  % divide figure, e.g. 1(a), 1(b)...

\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Information Dissamination \\ in Dynamic Networks \\ BACHELOR PROJEKT} % Title

\author{Till-Hagen \textsc{Mugele}\\ Alexander \textsc{Weinmann}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\begin{center}
\begin{tabular}{l r}
Supervisors: & Mohamad Ahmadi \\
			& Prof. Dr. Fabian Kuhn% Instructor/supervisor
\end{tabular}
\end{center}

% If you wish to include an abstract, uncomment the lines below
% \begin{abstract}
% Abstract text
% \end{abstract}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Problem Statement}
The network is represented by a complete graph of size n. Initially every node has a unique token. The Nodes of these graph adhere to the \textsc{congest} model with the additional constraint that each node can only send and receive $\mathcal{O}(\log(n))$ messages in every round. A node can only contact a random node in graph or a node which it already communicated with in the past. Additionally in each round an oblivious adversary can remove up to $\lceil\epsilon \cdot n\rceil$ nodes and replace them with new ones. The goal is to keep all the tokens alive with high probability for $\mathcal{O}(POLY(n))$ rounds. The challenge is to find a protocol that can keep the communication down (i.e. not violate the \textsc{CONGEST} model) and at the same time work against the differences that occur between the distribution of different tokens because of the variance effects of the token distributions.



\begin{description}
	\item[Network]
	The network is represented by a complete graph of size n. Initially every node has a unique token. These tokens are of size $\mathcal{O}(\log n)$. In practice this could be the unique ID of the node that had the token in round 0. This conveniently fits within the message size limitation of the \textsc{CONGEST} model.
	\item[Communication]
	In every round each node can send and receive $\mathcal{O}(\log n)$ messages. It can only send a message to a random node or to a node it has communicated with in the past. The message size must be $\mathcal{O}(\log n)$.
	\item[Churn]
	Additionally in each round an oblivious adversary can remove up to $\epsilon \cdot n$ nodes and replace them with new ones. The new nodes initially have no token. This is equivalent to wiping the memory of up to $\epsilon \cdot n$ nodes.
\end{description}

 
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Simulation Environment}

For this work we implemented a simulation environment where protocols that try to solve the afore-mentioned problem can be evaluated. Users can choose between pre-implemented churn scenarios or implement their own. The pre-implemented churns behave as follows:

\begin{description}
	\item[Churn0]
	Replaces $0$ nodes.
	\item[ChurnI]
	Replaces the $\epsilon \cdot n$ oldest nodes.
	\item[ChurnII]
	Replaces $\epsilon \cdot n$ random nodes.
	\item[ChurnIII]
	Replaces $X$ random nodes. Where the random variable $X$ is independent and distributed uniformly over the interval $[0,\epsilon \cdot n]$
	\item[ChurnIV]
	Replaces $\epsilon \cdot l^{r} \cdot n$ nodes. Where $l \in (0, 1]$ and $r$ is which round we are in. This allows us to have a decaying churn. Note that $l=1$ gives us ChurnII.
	\item[ChurnV]
	Replaces $\epsilon - \frac{\epsilon}{l^r} \cdot n$ nodes. Where $l \in [1, \infty)$ and $r$ is which round we are in. This allows us to have a increasing churn that is capped by $\epsilon$. Note that $l=1$ gives us ChurnII.
	\item[ChurnVI]
	Replaces $(\frac{\epsilon}{2}\cdot  - \cos(\frac{2\Pi}{l}r)+\frac{\epsilon}{2}) \cdot n$ nodes. Where $l \in [1, \infty)$ and $r$ is which round we are in. This allows us to have periodically increasing and decreasing churn.
\end{description}

The network is updated synchronously in every round. That is why we implemented the message manager. Each node computes the messages it wants to send and hands them of to the message manager. Once all nodes are done computing the message manager distributes all the messages. This way we ensure that no node gets additional information before the round ends. It is important to use the message manager when implementing your own protocols.

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Protocols}
The pseudo code for the presented algorithms (except AlgorithmIII) can be found in the appendix.

\subsection{AlgorithmI}
Let us define the age $a(n_{i})$ of a node $n_{i}$ as how many rounds the node has been in the network. Moreover we define $f(a(n_{i})) := min\{a(n_{i})\cdot\log n, \frac{n}{2}\}$ such that $f(a(n_{i}))$ is a reasonable approximation of the number of tokens known by node $n_{i}$. In round $r$ and for each other node $v$, node $u$ broadcasts each of its tokens to $v$ independently with probability $\frac{c\cdot \log n }{n \cdot f(a(u))}$. Whenever a node decides to send a token to another node, it implicitly creates a copy of the token and sends the copy. Upon receiving a copy $\tau_{i}$, a node only adds the copy to the set $T$ of token copies it has, when $\tau_{i} \notin T$.

\subsection{AlgorithmII}
Let $k$ be the number of tokens possessed by node $u$ in round $r$. For each other node $v$, node $u$ sends each of its tokens to $v$ independently with probability $\frac{c\cdot \log n}{n \cdot k}$. Whenever a node decides to send a token to another node, it implicitly creates a copy of the token and sends the copy. Upon receiving a copy $\tau_{i}$, a node only ads the copy to the set $T$ of token copies it has, when $\tau_{i} \notin T$.

\subsection{AlgorithmIII}
Let $\tau_{1}, \hdots, \tau_{n}$ be the tokens. During the protocol, only copies of the tokens are sent around. This means that when node $u$ decides to send a copy to node $v$, $u$ no longer has that copy. Each copy $c$ is a is a pair $(\tau_{i}, e_{c})$, where $\tau_{i}$ is the corresponding token and $e_{c}$ is called the effort value of this specific copy. Effort value of a copy means how much effort was put into sending this copy around since the beginning of the execution, and is precisely defined as follows. Considering $p(c, r)$ to be the probability of sending the copy $c$ in round $r$, the effort value of $c$ at the beginning of round $r'$ is $\sum_{j=1}^{r'-1}p(c,j)$. Since we have memory loss, we lose copies throughout the execution. Therefore, in addition to sending the copies around we need to also take care of a careful copy generation. We seperately discuss the following two aspects of the protocol:

\begin{description}
	\item[Copy Transmission]
	Let an arbitrary node $v$ have $s$ copies $(c_{1}, e_{1}),\hdots (c_{i}, e_{i})$ of a variety of tokens in round $r$. To each copy $(c_{i}, e_{i})$ it first assigns transmission probability $p_{i}$ such that $\sum_{i=1}^{s}p_{i} = \log n$, while minimizing the difference between the minimum and maximum effort values among the copies it has in a greedy manner. It increases each effort value $e_{i}$ by $p_{i}$. Then it tries to send each $(c_{i}, e_{i})$ with probability $p_{i}$.
	
	\item[Copy Generation]
	In the first round each node creates $\log n$ copies ot its token with effort value $1$ and sends them. In all other rounds the nodes decide independently from each other depending on the inspected churn whether to generate new copies. In every round each nodes decides with probability $1-\frac{y}{\mathcal{O}(\log n)}$ to replace each of its $(c_{i}, e_{i})$ by two $(c_{i}, e_{i})$'s, where $y$ is the number of tokens it has received in the previous round.
\end{description}

\subsection{AlgorithmIV}
For each token $\tau_{i}$ a node has it keeps a recency trace $\rho_{\tau_{i}}(r)$. In the first round $r=0$ each node only has one token $\tau_{i}$ and $\rho_{\tau_{i}}(0)=1$. At the beginning of each round the nodes update their recency trace as follows:
$$\rho_{\tau_{i}}(r+1)=\lambda \cdot \rho_{\tau_{i}}(r) + \emph{1}(\tau_{i} \in received(r))$$
$$\lambda \in (1,\infty)$$
We define $received(r)$ as the set of tokens a node received in round $r$ and the set of tokens a node has as $S$. Then the node sends each of its tokens $\tau_{i}$ to $p_{i}$ other random nodes in expectation.
$$p_{i} = c \cdot \log n \cdot \frac{\rho_{\tau_{i}}}{\sum_{\tau_{j} \in S}^{}\rho_{\tau_{j}}}$$

\subsection{AlgorithmV}
Let $T$ be the set of weighted tokens a node is holding.
Let us define the age $age(\tau_{i}, n_j)$ of a token $\tau_{i}$ as how many rounds the token has been held by the node $n_j$.\\
Under the assumption that nodes with larger a $T$ are more important to the network we define $w$ as $\frac{1}{1+|T| }+ \frac{1}{2}$.\\
For the first couple lowest $c$ tokens $\tau \in T$  with weight $< 1$ a node holds send $\tau$ with weight $w$ to $\log n$ random nodes in the network and increase the weight of $\tau$ by $w * log(n)$.
If a Node receives a token which it already holds increase the weight by $w * log(n)$ and reset the rounds the token has been hold by the node. Each Round, decrease the weight of each token by $\frac{1}{(1 + token\ age)} + \frac{1}{2}$





%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Results and Discussion}
\subsection{AlgorithmI}
The algorithm is not suitable for the problem because it either losses all tokens or sends more messages than it is allowed to. This twofold behaviour comes from the $min\{a(n_{i})\cdot\log n, \frac{n}{2}\}$. If the churn is high $\frac{n}{2}$ domintaes and if the churn is low $a\cdot \log n$ dominates. We now analyse the two cases:
\begin{description}
	\item[Case 1: $a\cdot \log n < \frac{n}{2}$]
	The probability to send a token is
	\begin{equation}
		\frac{c \log n}{a \log n} =\frac{c}{a}
	\end{equation}
	Since the expected age of a node is the same for any $n$ when using ChurnII, $\frac{c}{a}$ is constant with regards to variable $n$. However a constant number of messages is not enough to keep tokens alive for large $n$.
	
	\item[Case 2: $a\cdot \log n > \frac{n}{2}$]
	The adversary decreased the churn and now the algorithm sends too many messages.
\end{description}

In the following figures we used AlgorithmI with ChurnII and $\epsilon=0.005$. When we found a constant $c$ where the algorithm is able to keep all tokens alive we can increase the number of nodes and the tokens die out as we can see the in the figures below.

\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n250c0,84small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=250$, $c=0.84$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n250c1,01small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=250$, $c=1.01$}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n2500c0,84small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=2500$, $c=0.84$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n2500c1,01small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=2500$, $c=1.01$}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n25000c0,84small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=25000$, $c=0.84$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n25000c1,01small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=25000$, $c=1.01$}
	\end{minipage}
\end{figure}

In the following figures we used AlgorithmI with ChurnII. When we found a constant $c$ where the algorithm is able to hold all tokens alive we can increase  $\epsilon$ and the tokens die out as we can the in the figures below.

\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n500e0,005c0,84small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=0.84$, $\epsilon=0.005$ }
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n500e0,025c0,84small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=0.84$, $\epsilon=0.025$}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n500e0,025c1,3small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=1.3$, $\epsilon=0.025$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n500e0,06c1,3small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=1.3$, $\epsilon=0.06$}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n500e0,06c2,1small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=2.1$, $\epsilon=0.06$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/algo1n500e0,5c2,1small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=2.1$, $\epsilon=0.5$}
	\end{minipage}
\end{figure}

\subsection{AlgorithmII}
The algorithm is not suitable for the problem because it loses almost all tokens. The problem with this algorithm is that the variance in token distributions, i. e. one token has more copies in the network than others, is amplified each round. Once a token has more copies in the network than others, it has a higher probability to be sent around, resulting in even more copies. This results in tokens with a low number of copies not being sent and dying out.\\

In the following figures we used AlgorithmII with ChurnII and $\epsilon=0.1$. When we found a constant $c$ where the algorithm is able to keep all tokens alive we can increase the number of nodes and the token die out as we can the in the figures.

\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n125c2small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=125$, $c=2$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n125c2,5small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=125$, $c=2.5$}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n250c2small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=250$, $c=2$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n250c2,5small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=250$, $c=2.5$}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n500c2small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=2$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n500c2,5small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=500$, $c=2.5$}
	\end{minipage}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{minipage}[t]{.55\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n1000c2small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=1000$, $c=2$}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{.40\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.60]
		\begin{axis}[ 
		xlabel={rounds},
		ylabel={ percentage of alive tokens}]
		\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo2n1000c2,5small.csv};
		\end{axis}
		\end{tikzpicture}
		\captionof{figure}{$n=1000$, $c=2.5$}
	\end{minipage}
\end{figure}

\subsection{AlgorithmIII}
The algorithm is not suitable for the problem because it loses almost all tokens. We do not know why this algorithm behaves how it behaves.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
	\begin{axis}[
	xmode=log,
	xlabel={rounds},
	ylabel={percentage of alive tokens}]
	\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo3small.csv};
	\end{axis}
	\end{tikzpicture}
	\caption{AlgorithmIII: Parameters of this Simulation were: $n=400$, $\epsilon=0.05$, $c=1$, and ChurnII was used}
\end{figure}

\subsection{AlgorithmIV}
The idea of this algorithm is to inspect the churn and the distribution of different tokens because of the variance effects of the token distributions. For that we take a concept used in reinforcement learning, more precisely TD-Learning, called eligibility traces and modify it to apply to our problem. An appropriate name in our case would be recency traces. An example plot can be seen in Figure \ref{RecencyTrace}. Whenever a token has few copies in the network the recency traces of the nodes for this token are low, which means that it is sent more than tokens with more copies in the network. This ensures that tokens are equally distributed in the network. We do not know why it does not work.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
	\begin{axis}[ 
	xlabel={rounds},
	ylabel={$\rho_{\tau_{i}}$}]
	\addplot[only marks, mark size=0.5] table [x=round, y=recency, col sep=semicolon] {data/RecencyTrace.csv};
	\end{axis}
	\end{tikzpicture}
	\caption{Example recency trace $\rho_{\tau_{i}}$ of node $v$. Whenever $v$ receives $\tau_{i}$ the recency trace is increased by one after which it decays until it receives $\tau_{i}$ again.}\label{RecencyTrace}
\end{figure}


\begin{figure}[H]
	\centering
	\begin{tikzpicture}
	\begin{axis}[ 
	xlabel={rounds},
	ylabel={percentage of alive tokens}]
	\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo41small.csv};
	\end{axis}
	\end{tikzpicture}
	\caption{AlgorithmIV: Parameters of this Simulation were: $n=500$, $\epsilon=0.05$, $c=1.3$, $\lambda = 0.95$ and ChurnII was used}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
	\begin{axis}[ 
	xlabel={rounds},
	ylabel={percentage of alive tokens}]
	\addplot[only marks, mark size=0.5] table [x=round, y=percentage, col sep=semicolon] {data/Algo42small.csv};
	\end{axis}
	\end{tikzpicture}
	\caption{AlgorithmIV: Parameters of this Simulation were: $n=500$, $\epsilon=0.1$, $c=1.3$, $\lambda = 0.95$ and ChurnII was used}
\end{figure}

\subsection{AlgorithmV}

The idea was to keep the network form unnecessarily being flooded by tokens which are already stored by numerous nodes. To achieve this, it takes effect after a token was sent randomly to another node in the network. If the random node does not contain the token it will keep being distributed. If it does already contain the token, the distribution of it will cease for the time being. In this case, the probability with which any third node in the network also contains the token is high. 
With a message limit the Algorithm does not perform better than AlgorithmIV and is not able to hold all tokens alive.
If we disregard the message limit the algorithm can hold all tokens alive.

\begin{figure}[H]%
	\centering
	
	\subfloat[]{{
			\includegraphics[width=1.2\textwidth, angle=0]{data/tokensAlgo5}	
			
	}}%
	\qquad
	\subfloat[]{{
			\includegraphics[width=1.2\textwidth, angle=0]{data/messagesAlgo5}	
			
	}}%
	\qquad
	
	
	\caption{AlgorithmV: Parameters of this Simulation were: $n=400$, $\epsilon=0.5$, $c=\infty$ and ChurnVI was used}
	\label{algo5sinus}%
\end{figure}

As we can see in Figure 25 the number of messages decreases and increases depending on the churn rate. What is nice about this algorithm is that it decreases the number of messages when the churn is low and increases the number of messages when the churn is high, even with extreme change in churn rate. Maybe it would be interesting to know whether the number of messages decreases in relation to n with bigger n and still hold this effect.




%----------------------------------------------------------------------------------------
%	SECTION 5
%----------------------------------------------------------------------------------------

\section{Future Work}
Analyse the new proposed algorithms in detail. For example for AlgorithmIV and AlgorithmV we think that all tokens are distributed evenly in the network, however this needs to be checked.
Furthermore there could be be other churn scenarios. When it comes to real world applications a lot of things behave periodically so a $sin(time)$ churn could be appropriate. Also a power-law distributed churn could be interesting to investigate and how nodes deal with rare drops or spikes in the churn rate.

\section{Appendix}


\begin{algorithm}
	\caption*{AlgorithmI}
	\begin{algorithmic}[1]		
		\Function{Send}{}
		\ForAll{$v \in V$}
		\ForAll{$\tau_{i} \in T$}
		\State send $\tau_{i}$ to $v$ with probability $\frac{c\cdot \log n}{n\cdot f(a(u))}$
		\EndFor
		\EndFor
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\begin{algorithm}
	\caption*{AlgorithmII}\label{algo2}
	\begin{algorithmic}[1]		
		\Function{Send}{}
		\ForAll{$v \in V$}
		\ForAll{$\tau_{i} \in T$}
		\State send $\tau_{i}$ to $v$ with probability $\frac{c\cdot \log n}{n\cdot k}$
		\EndFor
		\EndFor
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\begin{algorithm}
	\caption*{AlgorithmIV}\label{algo4}
	\begin{algorithmic}[1]
		\Function{Receive}{$\tau_{i}$}
		\State $\rho_{\tau_{i}} \gets \rho_{\tau_{i}}+1$	
		\EndFunction
		
		\Function{Send}{$\tau_{i}$}
		\ForAll{$\tau_{i} \in T$}
		\State $\rho_{\tau_{i}} \gets \lambda * \rho_{\tau_{i}}$ 	
		\EndFor
		\State $p_{i} \gets c \cdot \log n \cdot \frac{\frac{1}{\rho_{\tau_{i}}}}{\sum_{j=0}^{|S|}\frac{1}{\rho_{\tau_{j}}}}$
		
		\ForAll{$\tau_{i} \in T$}
		\State $j \gets p_{i}$
		\While{$j > 0$}
		\If{$j\leq 1$}
			\State send $\tau_{i}$ to a random node with probability $j$
		\Else
		
		
		\State send $\tau_{i}$ to a random node
		\EndIf
		
		\State $j \gets j-1$
		\EndWhile
		\EndFor
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\begin{algorithm}
	\caption*{AlgorithmV}\label{algo5}
	\begin{algorithmic}[1]	
		\Function{Send}{}
		\ForAll{$v_i \in V$}
		\State $i, j = 0, w =\frac{1}{1+|T_i| }+ \frac{1}{2}$
		\While{$i < c$}
		\State $i \gets i+1$
		\For{$\tau \in T_i\ with\ lowest\ weight < 1$}
		\While{$j < \log n$}
		\State $j \gets j+1$
		\State send $\tau$  to a random $ v_j \in V$ with weight $w$ 
		\If{$\tau \in T_j$ }
		\State Increase $\tau \in T_j$ weight by $w * \log n$
		\State $(rounds\ \tau\ holding) = 1$ 
				\Else
				\State Set $\tau \in T_j$ weight to $w$
		\EndIf
		\EndWhile
		\State Increase $\tau \in T_i$ weight by $w * \log n$
		\EndFor
		\EndWhile
		\EndFor
		\EndFunction
		\Function{Decrease}{}
		\ForAll{$v \in V$}
		\ForAll{$\tau \in T$}
		\State decrease weight of $\tau$ by $\frac{1}{(rounds\ \tau\ holding)} + \frac{1}{2}$
		\EndFor
		\EndFor
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\end{document}