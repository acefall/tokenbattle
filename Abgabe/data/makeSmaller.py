import sys

if len(sys.argv) != 3:
    print("Usage: makeSmaller.py <file to reduce without .csv ending> <lines to skip>")
    exit()

with open(sys.argv[1]+".csv", 'r') as source:
    with open(sys.argv[1]+"small.csv", 'w') as target:
        lines= source.readlines()
        for i, line in enumerate(lines):
            if i == 0:
                target.write(line)
            if i % int(sys.argv[2]) == 1:
                target.write(line)


