﻿using System;
using System.Diagnostics;
using System.IO;
using TokenBattleCSharp.Churns;

namespace TokenBattleCSharp
{
    public sealed class TokenBattle
    {
        internal TokenBattle(Strategy pStrategy)
        {
            mStrategy = pStrategy;
        }

        #region Properties

        public static Random Random { get; } = new Random(1);

        #endregion Properties


        public void Simulate(int pRounds, int _nodes, string pFileName, bool pDebug = false, bool pLog = true)
        {
            var graph = new Graph(_nodes, mStrategy);
            var messageMgr = new MessageManager(graph);
            var churn = ChurnFactory.Get(mStrategy.ChurnKey);

            using (var sw = new StreamWriter("./Visualisation/Data/" + pFileName))
            {
                sw.WriteLine(graph.Nodes.Length
                             + ";" + pRounds
                             + ";" + mStrategy.Epsilon
                             + ";" + mStrategy.Constant
                             + ";" + mStrategy.Lambda
                             + ";" + mStrategy.AlgorithmKey
                             + ";" + mStrategy.ChurnKey
                             + ";" + mStrategy.MessageLimit);

                var stopWatchTotal = new Stopwatch();
                stopWatchTotal.Start();

                messageMgr.Initialze();
                for (var i = 0; i < pRounds; i++)
                {
                    var stopWatch = new Stopwatch();
                    stopWatch.Start();

                    graph.Round++;
                    if (pDebug)
                    {
                        Console.WriteLine("Round: " + graph.Round + "::::::::::::::::::::::::::::::::::::::::\n");
                    }


                    mMessagesCount = messageMgr.CollectMessages();
                    messageMgr.DeliverMessages();
                    if (pDebug)
                    {
                        Console.WriteLine("Messages done after " + stopWatch.Elapsed.Milliseconds + " seconds!\n");
                    } //O(n^3)

                    var killed = churn.Churn(graph, mStrategy.Epsilon);
                    if (pDebug)
                    {
                        Console.WriteLine("Churn done after " + stopWatch.Elapsed.Seconds + " seconds!\n");
                    }

                    if (pLog)
                    {
                        sw.WriteLine(graph.ToCsv(mMessagesCount, killed));
                    }


                    if (mMessagesCount.Item2 > mStrategy.MessageLimit)
                    {
                        Console.WriteLine("Limit breached! Ends Simulation after Round: " + graph.Round);
                        break;
                    }
                }

                stopWatchTotal.Stop();
                Console.WriteLine("Simulation with epsilon:" + mStrategy.Epsilon + " and constant:" +
                                  mStrategy.Constant + " Done in " + stopWatchTotal.Elapsed.TotalSeconds +
                                  " seconds!\n");
            }
        }

        #region Fields

        private readonly Strategy mStrategy;
        private Tuple<int, int> mMessagesCount;

        #endregion Fields
    }
}