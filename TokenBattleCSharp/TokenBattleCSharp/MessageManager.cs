﻿using System;
using System.Collections.Generic;
using TokenBattleCSharp.Algorithms;

namespace TokenBattleCSharp
{
    public class MessageManager
    {
        private readonly Graph mGraph;
        private readonly Dictionary<Node, List<IMessage>> mToSendMessages = new Dictionary<Node, List<IMessage>>();

        public MessageManager(Graph _graph)
        {
            mGraph = _graph;
        }

        public void Initialze()
        {
            foreach (var node in mGraph.Nodes)
            {
                node.Algorithm.Initialize();
            }
        }

        public Tuple<int, int> CollectMessages()
        {
            var messageComplexity = 0;
            var maxMessageComplexity = 0;
            foreach (var node in mGraph.Nodes)
            {
                var toSend = node.Algorithm.Send();

                foreach (var message in toSend)
                {
                    if (!mToSendMessages.ContainsKey(message.Receiver))
                    {
                        mToSendMessages[message.Receiver] = new List<IMessage>();
                    }

                    mToSendMessages[message.Receiver].Add(message);
                }

                messageComplexity += toSend.Count;
                maxMessageComplexity = Math.Max(maxMessageComplexity, toSend.Count);
            }

            return new Tuple<int, int>(messageComplexity, maxMessageComplexity);
        }

        public void DeliverMessages()
        {
            foreach (var pair in mToSendMessages)
            {
                pair.Key.Algorithm.Receive(pair.Value);
            }

            mToSendMessages.Clear();
        }
    }
}