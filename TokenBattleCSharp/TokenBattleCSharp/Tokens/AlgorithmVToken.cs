﻿namespace TokenBattleCSharp.Tokens
{
    public class AlgorithmVToken : Token
    {
        public AlgorithmVToken(int _id, double _weight) : base(_id)
        {
            Weight = _weight;
        }

        public double Weight { get; set; }
        public int RoundsHolding { get; set; } = 0; // Only used in algorithmV.cs
    }
}