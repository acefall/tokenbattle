﻿namespace TokenBattleCSharp.Tokens
{
    public class Token
    {
        public Token(int _id)
        {
            Id = _id;
        }

        public int Id { get; }
    }
}