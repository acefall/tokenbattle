﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using TokenBattleCSharp.Algorithms;

namespace TokenBattleCSharp
{
    public sealed class Graph
    {
        #region Fields

        public Dictionary<int, int> TokenCounter { get; } = new Dictionary<int, int>();

        public Dictionary<int, int> NodeId2Index { get; } = new Dictionary<int, int>();

        public Node[] Nodes { get; set; }

        public int Size => Nodes.Length;

        public int Oldest { get; set; }

        public int Round { get; set; }

        public Strategy Strategy { get; }

        #endregion Fields

        #region Methodes

        public Graph(int _nodeCount, Strategy pStrategy)
        {
            Strategy = pStrategy;
            Nodes = new Node[_nodeCount];
            Generate(_nodeCount);
        }

        public void Generate(int _nodeCount)
        {
            Oldest = 0;
            for (var i = 0; i < _nodeCount; i++)
            {
                var node = new Node(Round, this);
                Nodes[i] = node;
                NodeId2Index[node.Id] = i;
            }
        }

        public void Replace(Node _node)
        {
            // Get new Id
            var newId = _node.NextId();

            // Update Data structure
            var index = NodeId2Index[_node.Id];
            NodeId2Index.Remove(_node.Id);
            NodeId2Index[newId] = index;

            // reset propteries of the node
            Nodes[index].Birthday = Round;
            Nodes[index].Id = newId;

            _node.Algorithm.DeleteTokens();

            var algoFactory = new AlgorithmFactory();
            _node.Algorithm = algoFactory.Get(_node.Graph.Strategy.AlgorithmKey, _node);
        }


        // Return String "tokens;epsilon;#totalmessages;#maxMessages"
        public string ToCsv(Tuple<int, int> pMessagesCount, double _killPercentage)
        {
            var res = "";
            double aliveTokens = 0;
            int totalTokens = 0;
            foreach (var item in TokenCounter)
            {
                totalTokens += item.Value;
                if (item.Value > 0)
                {
                    aliveTokens++;
                }
            }

            res = Round+";" +(aliveTokens/TokenCounter.Count).ToString(CultureInfo.InvariantCulture)+";"+totalTokens+";" + _killPercentage.ToString(CultureInfo.InvariantCulture) + ";" + pMessagesCount.Item1 + ";" + pMessagesCount.Item2;
            return res;
        }

        #endregion Methods
    }
}