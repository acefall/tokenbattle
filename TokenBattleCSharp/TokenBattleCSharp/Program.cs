﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace TokenBattleCSharp
{
    internal class Program
    {
        private static readonly int nodeCount = 400;
        private static readonly int roundCount = 40000;
        private static readonly int maxMessages = roundCount;

        private static void Main(string[] args)
        {
            var paramsTuples = new List<Strategy>();
            var batchTimestamp = DateTime.Now;

            double[] constants = {1};
            double[] lambdas = { 5000 };
            double[] epsilons = {0.5};
            int[] algorithmKeys = {5};
            int[] churnKeys = {6};

            foreach (var algorithmKey in algorithmKeys)
            {
                foreach (var churnKey in churnKeys)
                {
                    foreach (var epsilon in epsilons)
                    {
                        foreach (var constant in constants)
                        {
                            foreach (var lambda in lambdas)
                            {
                                paramsTuples.Add(new Strategy(algorithmKey,
                                    churnKey,
                                    epsilon,
                                    constant,
                                    lambda,
                                    maxMessages,
                                    batchTimestamp));
                            }
                        }
                    }
                }
            }


            Parallel.ForEach(paramsTuples, DoWork);


            //DoWork(paramsTuples[0]);
        }

        // pParamstuple = (algorithmKey, churnKey, epsilon, constant, batchTimestamp)
        private static void DoWork(Strategy _strategy)
        {
            var nodes = nodeCount;
            var rounds = roundCount;

            var batchTimestamp = _strategy.BatchTimeStamp.ToString("MMdd_HHmmss") + "_n;" + nodes + "_r;" + rounds;

            var filename = batchTimestamp + "/"
                                          + "algo;" + _strategy.AlgorithmKey
                                          + "_churn;" + _strategy.ChurnKey
                                          + "_e;" + _strategy.Epsilon
                                          + "_c;" + _strategy.Constant
                                          + "_lambda;" + _strategy.Lambda
                                          + ".csv";
            Directory.CreateDirectory("./Visualisation/Data/" + batchTimestamp);

            var strategy = _strategy;

            var tokenBattle = new TokenBattle(strategy);

            tokenBattle.Simulate(rounds, nodes, filename, false, true);
        }
    }
}