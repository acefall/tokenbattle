﻿using TokenBattleCSharp.Tokens;

namespace TokenBattleCSharp.Algorithms
{
    public interface IMessage
    {
        Node Sender { get; }
        Node Receiver { get; }
        Token Token { get; }
    }
}