﻿using System;

namespace TokenBattleCSharp.Algorithms
{
    public class AlgorithmFactory
    {
        public IAlgorithm Get(int id, Node _node)
        {
            switch (id)
            {
                case 1:
                    return new AlgorithmI(_node);
                case 2:
                    return new AlgorithmII(_node);
                case 3:
                    return new AlgorithmIII(_node);
                case 4:
                    return new AlgorithmIV(_node);
                case 5:
                    return new AlgorithmV(_node);
                case 6:
                    return new AlgorithmVI(_node);
                default:
                    throw new InvalidOperationException("AlgorithmKey not found!");
            }
        }
    }
}