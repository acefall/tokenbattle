﻿using TokenBattleCSharp.Tokens;

namespace TokenBattleCSharp.Algorithms
{
    public class Message : IMessage
    {
        public Message(Node _sender, Node _receiver, int _token)
        {
            Sender = _sender;
            Receiver = _receiver;
            Token = new Token(_token);
        }

        public Node Sender { get; }
        public Node Receiver { get; }
        public Token Token { get; }
    }
}