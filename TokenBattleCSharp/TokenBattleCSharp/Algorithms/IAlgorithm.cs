﻿using System.Collections.Generic;

namespace TokenBattleCSharp.Algorithms
{
    public interface IAlgorithm
    {
        void Initialize();
        List<IMessage> Send();
        void Receive(List<IMessage> _messages);

        void DeleteTokens();
    }
}