﻿using System;
using System.Collections.Generic;

namespace TokenBattleCSharp.Algorithms
{
    internal sealed class AlgorithmI : IAlgorithm
    {
        public AlgorithmI(Node _node)
        {
            Tokens = new HashSet<int>();
            Node = _node;
        }

        private HashSet<int> Tokens { get; }
        private Node Node { get; }

        public void Initialize()
        {
            Tokens.Add(Node.Id);
            Node.Graph.TokenCounter[Node.Id] = 1;
            var toSendMessages = new List<IMessage>();
            for (int i = 0; i < Math.Log(Node.Graph.Size); i++)
            {
                var randomNode = Node.GetRandomNeighbour();
                toSendMessages.Add(new Message(Node, randomNode, Node.Id));
            }
        }

        public List<IMessage> Send()
        {
            var toSendMessages = new List<IMessage>();
            var graph = Node.Graph;

            var probability = graph.Strategy.Constant * Math.Log(graph.Size) /
                              F(graph.Round - Node.Birthday, graph.Size);


            //for each other node v, _node sends each of its tokens to v independently.
            foreach (var token in Tokens)
            {
                var n = 0;
                while (0 < probability - n)
                {
                    if (Node.Random.NextDouble() < probability)
                    {
                        var randomNode = Node.GetRandomNeighbour();
                        toSendMessages.Add(new Message(Node, randomNode, token));
                    }

                    n++;
                }
            }

            return toSendMessages;
        }

        public void Receive(List<IMessage> _messages)
        {
            foreach (var message in _messages)
            {
                var algo = (AlgorithmI) message.Receiver.Algorithm;
                if (!algo.Tokens.Contains(message.Token.Id))
                {
                    algo.Tokens.Add(message.Token.Id);
                    Node.Graph.TokenCounter[message.Token.Id]++;
                }
            }
        }

        public void DeleteTokens()
        {
            foreach (var token in Tokens)
            {
                Node.Graph.TokenCounter[token]--;
            }
        }

        //Approximate number of tokens
        private double F(int a, int pGraphSize)
        {
            return Math.Min(a * Math.Log(pGraphSize), pGraphSize / 2.0);
        }
    }
}