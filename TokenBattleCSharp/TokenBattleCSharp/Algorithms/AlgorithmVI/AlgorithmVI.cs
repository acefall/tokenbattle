﻿using System;
using System.Collections.Generic;
using TokenBattleCSharp.Tokens;

namespace TokenBattleCSharp.Algorithms
{
    public class AlgorithmVI : AlgorithmV
    {
        public AlgorithmVI(Node _node) : base(_node)
        {
        }

        public override List<IMessage> Send()
        {
            var toSendMessages = new List<IMessage>();
            if (Tokens.Count == 0)
            {
                return toSendMessages;
            }

            var newWeight = (double) 1 / (1 + Tokens.Count) + (double) 1 / 2;
            var tokenToSend = new List<Tuple<AlgorithmVToken, double>>();
            double sum = 0;

            foreach (var token in Tokens.Values)
            {
                if (token.Weight < 1)
                {
                    var probability = 2 - token.Weight * 2; // so 0.5 = 1 and 1 = 0;
                    sum += probability;
                    tokenToSend.Add(Tuple.Create(token, probability));
                }
            }

            if (tokenToSend.Count == 0)
            {
                double sum2 = 0;

                foreach (var token in Tokens.Values)
                {
                    sum2 += token.Weight;
                }

                foreach (var token in Tokens.Values)
                {
                    var probability = 1 - token.Weight / sum2;
                    sum += probability;
                    tokenToSend.Add(Tuple.Create(token, probability));
                }
            }

            //sending
            for (var i = 0; i <= Math.Log(Node.Graph.Size) * Node.Graph.Strategy.Constant; i++)
            {
                foreach (var pair in tokenToSend)
                {
                    if (Node.Random.NextDouble() < pair.Item2 / sum)
                    {
                        var token = pair.Item1;
                        toSendMessages.Add(new AlgorithmVMessage(Node,
                            Node.GetRandomNeighbour(),
                            new AlgorithmVToken(token.Id, newWeight)));
                        Tokens[token.Id].Weight += newWeight;
                    }
                }
            }

            foreach (var entry in Tokens)
            {
                var token = entry.Value; // get token.

                token.RoundsHolding += 1;

                //decrease token weight 
                var x = (double) 1 / (1 + token.RoundsHolding) + 1 / 2;
                var newValues = token.Weight - x;
                token.Weight = Math.Max(newValues, 0.5); // (1 / 1+n) + 1/2.
            }

            return toSendMessages;
        }
    }
}