﻿using System;

namespace TokenBattleCSharp.Algorithms
{
    internal class AlgorithmIIIMessage : Message
    {
        public AlgorithmIIIMessage(Node _sender, Node _receiver, Tuple<int, double> _token) : base(_sender,
            _receiver,
            _token.Item1)
        {
            Weight = _token.Item2;
        }

        public double Weight { get; }
    }
}