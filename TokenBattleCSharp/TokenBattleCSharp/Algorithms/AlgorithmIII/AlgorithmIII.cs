﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TokenBattleCSharp.Algorithms
{
    internal class AlgorithmIII : IAlgorithm
    {
        private int mReceivedMessagesLastRound;

        public AlgorithmIII(Node _node)
        {
            Node = _node;
            EffortValues = new List<Tuple<int, double>>();
        }

        private List<Tuple<int, double>> EffortValues { get; set; }
        private Node Node { get; }

        public void Initialize()
        {
            EffortValues.Add(new Tuple<int, double>(Node.Id, 0));
            Node.Graph.TokenCounter[Node.Id] = 1;
        }

        public List<IMessage> Send()
        {
            CollapseEffortValues();
            CopyGeneration();
            return CopyTransmission(CalculateNewEffortValues());
        }

        public void Receive(List<IMessage> _messages)
        {
            mReceivedMessagesLastRound = _messages.Count;
            foreach (AlgorithmIIIMessage message in _messages)
            {
                EffortValues.Add(new Tuple<int, double>(message.Token.Id, message.Weight));
            }
        }

        public void DeleteTokens()
        {
            foreach (var token in EffortValues)
            {
                Node.Graph.TokenCounter[token.Item1]--;
            }
        }

        /// <summary>
        ///     Calculates the transmission probability by minimizing the diffence between the minimum and maximum effortvalues
        ///     among the copies.
        /// </summary>
        private List<double> CalculateNewEffortValues()
        {
            //Exit if node has no tokens
            if (EffortValues.Count <= 0)
            {
                return new List<double>();
            } //catch no tokens to send.


            var transmissionProbabilities = new List<double>(); // probabilities
            var graph = Node.Graph; // graph reference
            var budget = Math.Log(graph.Size) * Node.Graph.Strategy.Constant; // budget

            // minimize the diffence between the minimum and maximum effortvalues among the copies.
            // _node.TokenEfforts TokenForrots is sorted.
            var i = 0;
            while (budget > 0)
            {
                var effortChange = budget / (i + 1); //value by which the lowest effortvalue(s) will be increased.
                var difference =
                    EffortValues[0].Item2 - EffortValues[i].Item2; //diffrence between the i'th and i+1'th effortvalue.

                //if all tokencopies have the same effortvalue.
                if (i >= EffortValues.Count - 1)
                {
                    //foreach tokencopy increase effortvalue and transmission probability by effortChange
                    for (var k = 0; k <= i; k++)
                    {
                        //Set new effortValue
                        EffortValues[k] =
                            new Tuple<int, double>(EffortValues[k].Item1, EffortValues[k].Item2 + effortChange);


                        //if the entry already exists.
                        if (transmissionProbabilities.Count >= k + 1)
                        {
                            transmissionProbabilities[k] += effortChange;
                        }
                        else
                        {
                            transmissionProbabilities.Add(effortChange);
                        }
                    }

                    break;
                }
                //first difference in effortvalues between tokencopies. 

                if (difference > 0)
                {
                    if (difference >= budget)
                    {
                        //distribute rest of the budget evenly over the lowest effortvalues.
                        for (var k = 0; k < i; k++)
                        {
                            //Set new effortValue
                            EffortValues[k] = new Tuple<int, double>(EffortValues[k].Item1,
                                EffortValues[k].Item2 + effortChange);

                            //if the entry already exists.
                            if (transmissionProbabilities.Count >= k + 1)
                            {
                                transmissionProbabilities[k] += effortChange;
                            }
                            else
                            {
                                transmissionProbabilities.Add(effortChange);
                            }
                        }

                        break;
                    }
                    //If budget > difference between the smallest and second smallest.

                    for (var k = 0; k < i; k++)
                    {
                        EffortValues[k] = new Tuple<int, double>(EffortValues[k].Item1,
                            EffortValues[k].Item2 + difference);

                        //if the entry already exists
                        if (transmissionProbabilities.Count >= k + 1)
                        {
                            transmissionProbabilities[k] += difference;
                        }
                        else
                        {
                            transmissionProbabilities.Add(difference);
                        }

                        budget -= difference;
                    }
                }

                i++;
            }

            return transmissionProbabilities;
        }

        /// <summary>
        ///     Sends each tokencopy with probability to a random node.
        /// </summary>
        private List<IMessage> CopyTransmission(List<double> pTransmissionProbability)
        {
            var toSendMessages = new List<IMessage>();
            var toDeleteTokens = new List<Tuple<int, double>>();

            //foreach tokencopy send with probability to a random node.
            for (var j = 0; j < pTransmissionProbability.Count; j++) // O(n) times
            {
                if (Node.Random.NextDouble() < pTransmissionProbability[j])
                {
                    var token = EffortValues[j];
                    toSendMessages.Add(new AlgorithmIIIMessage(Node, Node.GetRandomNeighbour(), token));
                    toDeleteTokens.Add(token);
                }
            }

            foreach (var token in toDeleteTokens)
            {
                //Delete Tokencopy from sender
                EffortValues.Remove(token);
            }

            return toSendMessages;
        }

        /// <summary>
        ///     Create noe copies of all the copies a node has with probability 1-(y/O(logn)) where y is the number of tokens it
        ///     has received in the last round
        /// </summary>
        public void CopyGeneration()
        {
            // Probabilty with wich all copies are copied
            var probability = 1 - mReceivedMessagesLastRound / (Math.Log(Node.Graph.Size) * Node.Graph.Strategy.Lambda);
            probability = Math.Max(0, probability);

            mReceivedMessagesLastRound = 0; // resets the counter

            // Each node creates log(n) copies of its token with effort value 1
            if (Node.Graph.Round == 1)
            {
                for (var i = 0; i < Math.Log(Node.Graph.Nodes.Length); i++)
                {
                    var token = new Tuple<int, double>(EffortValues[0].Item1, EffortValues[0].Item2);
                    EffortValues.Add(token);
                    Node.Graph.TokenCounter[token.Item1]++;
                }
            }
            else
            {
                if (Node.Random.NextDouble() < probability)
                {
                    foreach (var token in EffortValues)
                    {
                        Node.Graph.TokenCounter[token.Item1]++;
                    }

                    // Copy each token copy
                    //_node.EffortValues.AddRange(_node.EffortValues.Select(pair => Tuple.Create<int, double>(pair.Item1, pair.Item2)).ToList());
                    EffortValues.AddRange(EffortValues.ToList());
                }
            }
        }

        private void CollapseEffortValues()
        {
            var sumDic = new Dictionary<int, double>();

            foreach (var tuple in EffortValues)
            {
                if (sumDic.ContainsKey(tuple
                    .Item1))
                {
                    sumDic[tuple.Item1] += tuple.Item2;
                    Node.Graph.TokenCounter[tuple.Item1]--;
                }
                else
                {
                    sumDic[tuple.Item1] = tuple.Item2;
                }
            }

            //convert dic to list
            EffortValues = sumDic.Select(pair => Tuple.Create(pair.Key, pair.Value)).ToList();
        }
    }
}