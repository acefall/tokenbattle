﻿using System;
using System.Collections.Generic;

namespace TokenBattleCSharp.Algorithms
{
    public class AlgorithmIV : IAlgorithm
    {
        public AlgorithmIV(Node _node)
        {
            Node = _node;
            RecencyTraces = new Dictionary<int, double>();
        }

        private Dictionary<int, double> RecencyTraces { get; }
        private Node Node { get; }

        public void Initialize()
        {
            RecencyTraces[Node.Id] = 1;
            Node.Graph.TokenCounter[Node.Id] = 1;
        }

        public List<IMessage> Send()
        {
            var toSendMessages = new List<IMessage>();
            if (RecencyTraces.Count == 0)
            {
                return toSendMessages;
            }

            // Decay recency trace
            var keys = new List<int>(RecencyTraces.Keys);
            foreach (var key in keys)
            {
                RecencyTraces[key] =
                    Node.Graph.Strategy.Lambda * RecencyTraces[key];
            }

            // Sum for normalization
            double sum = 0;
            Dictionary<int, double> multInverse = new Dictionary<int, double>();
            foreach (var pair in RecencyTraces)
            {
                multInverse[pair.Key] = 1 / pair.Value;
                sum += multInverse[pair.Key];
            }

            // Calculate probabilities by normalizing the multiplicative inverse
            var p = new Dictionary<int, double>();
            foreach (var pair in multInverse)
            {
                p[pair.Key] = Node.Graph.Strategy.Constant * Math.Log(Node.Graph.Size) * pair.Value / sum;
            }


            foreach (var pair in p)
            {
                var value = pair.Value;
                // Sending tokens
                do
                {
                    if (Node.Random.NextDouble() < value)
                    {
                        toSendMessages.Add(new Message(Node, Node.GetRandomNeighbour(), pair.Key));
                    }

                    value--;
                } while (value > 0);
            }


            return toSendMessages;
        }

        public void Receive(List<IMessage> _messages)
        {
            foreach (var message in _messages)
            {
                if (!RecencyTraces.ContainsKey(message.Token.Id))
                {
                    RecencyTraces[message.Token.Id] = 0;
                    Node.Graph.TokenCounter[message.Token.Id]++;
                }

                RecencyTraces[message.Token.Id] += 1;
            }
        }

        public void DeleteTokens()
        {
            foreach (var token in RecencyTraces.Keys)
            {
                Node.Graph.TokenCounter[token]--;
            }
        }
    }
}