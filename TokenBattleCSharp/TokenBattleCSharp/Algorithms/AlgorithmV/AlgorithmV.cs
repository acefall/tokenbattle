﻿using System;
using System.Collections.Generic;
using TokenBattleCSharp.Tokens;

namespace TokenBattleCSharp.Algorithms
{
    public class AlgorithmV : IAlgorithm
    {
        public AlgorithmV(Node _node)
        {
            Node = _node;
            Tokens = new Dictionary<int, AlgorithmVToken>();
        }

        protected Node Node { get; }
        protected Dictionary<int, AlgorithmVToken> Tokens { get; }

        public void Initialize()
        {
            Tokens[Node.Id] = new AlgorithmVToken(Node.Id, 0.5);
            Node.Graph.TokenCounter[Node.Id] = 1;
        }

        public virtual List<IMessage> Send()
        {
            var toSendMessages = new List<IMessage>();
            if (Tokens.Count == 0)
            {
                return toSendMessages;
            }

            var newWeight = (double) 1 / (1 + Tokens.Count) + (double) 1 / 2;

            foreach (var pair in Tokens)
            {
                var token = pair.Value;
                if (token.Weight < 1)
                {
                    var y = 1 - token.Weight;

                    var z = 0.5 / y;

                    var propability = Math.Log(Node.Graph.Size) / z; // log(n) / (  0.5 / 1 - weight).

                    var n = 0;
                    do // will be called at least once at most log(n)
                    {
                        toSendMessages.Add(new AlgorithmVMessage(Node,
                            Node.GetRandomNeighbour(),
                            new AlgorithmVToken(token.Id, newWeight)));
                        Tokens[token.Id].Weight += newWeight;
                        n++;
                    } while (n < propability);
                }
            }

            foreach (var entry in Tokens)
            {
                var token = entry.Value; // get token.

                token.RoundsHolding += 1;

                //decrease token weight 
                var x = (double)1 / (1 + token.RoundsHolding) + 1 / 2;
                var newValues = token.Weight - x;
                token.Weight = Math.Max(newValues, 0.5); // (1 / 1+n) + 1/2.
            }

            return toSendMessages;
        }


        public void Receive(List<IMessage> _messages)
        {
            foreach (AlgorithmVMessage message in _messages)
            {
                if (Tokens.ContainsKey(message.Token.Id)) //if randomNode already holds the token.
                {
                    Tokens[message.Token.Id].Weight +=
                        Math.Log(Node.Graph.Size) *
                        message.Token.Weight; // increase the weight of the token hold by the randomNode by 
                }
                else
                {
                    Tokens[message.Token.Id] = new AlgorithmVToken(message.Token.Id, message.Token.Weight);
                    Node.Graph.TokenCounter[message.Token.Id]++;
                }
            }
        }

        public void DeleteTokens()
        {
            foreach (var token in Tokens.Keys)
            {
                Node.Graph.TokenCounter[token]--;
            }
        }
    }
}