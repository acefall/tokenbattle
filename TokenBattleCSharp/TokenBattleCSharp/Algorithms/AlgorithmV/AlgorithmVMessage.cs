﻿using TokenBattleCSharp.Tokens;

namespace TokenBattleCSharp.Algorithms
{
    internal class AlgorithmVMessage : Message
    {
        public AlgorithmVMessage(Node _sender, Node _receiver, AlgorithmVToken _token) : base(_sender,
            _receiver,
            _token.Id)
        {
            Token = _token;
        }

        public new AlgorithmVToken Token { get; }
    }
}