﻿using System;
using TokenBattleCSharp.Algorithms;

namespace TokenBattleCSharp
{
    public class Node
    {
        #region Properties

        public Graph Graph { get; }
        public Random Random { get; }
        public int Birthday { get; set; }
        public int Id { get; set; }

        public IAlgorithm Algorithm { get; set; }

        // Used for generation of unique ids
        public static int sCounterId = -1;

        #endregion

        #region Methods

        public Node(int _birthday, Graph _graph)
        {
            Id = NextId();
            Birthday = _birthday;
            Graph = _graph;
            Random = new Random(Id + _graph.Round);
            Algorithm = new AlgorithmFactory().Get(Graph.Strategy.AlgorithmKey, this);
        }


        public override string ToString()
        {
            return "[N: " + Id + " Birthday: " + Birthday + "]";
        }

        public Node GetRandomNeighbour()
        {
            var neighbour = Graph.Nodes[Random.Next(Graph.Nodes.Length)];
            if (neighbour.Id == Id)
            {
                return GetRandomNeighbour();
            }

            return neighbour;
        }

        public int NextId()
        {
            sCounterId++;
            return sCounterId;
        }

        #endregion Methods
    }
}