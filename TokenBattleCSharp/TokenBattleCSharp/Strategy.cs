﻿using System;

namespace TokenBattleCSharp
{
    public class Strategy
    {
        public Strategy(int _algorithmKey,
            int _churnKey,
            double _epsilon,
            double _constant,
            double lambda,
            int _messageLimit,
            DateTime _batchTimeStamp)
        {
            Epsilon = _epsilon;
            Constant = _constant;
            Lambda = lambda;
            AlgorithmKey = _algorithmKey;
            ChurnKey = _churnKey;
            MessageLimit = _messageLimit;
            BatchTimeStamp = _batchTimeStamp;
        }

        #region Fields

        public double Constant { get; }
        public double Lambda { get; }
        public double Epsilon { get; }
        public int AlgorithmKey { get; }
        public int ChurnKey { get; }
        public int MessageLimit { get; }

        public DateTime BatchTimeStamp { get; }

        #endregion Fields
    }
}