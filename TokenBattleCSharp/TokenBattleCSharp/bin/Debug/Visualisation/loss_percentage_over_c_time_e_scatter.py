import sys
import datetime
from pprint import pprint
import plotly.offline as py
import plotly.graph_objs as go
import webbrowser, os
import numpy as np
import re

from analytics import Analytics

"""3d-scatter-plot of loss percentage over time over c, different traces for e"""
def loss_percent_c_rounds_e(pData, pAlgo, pChurn):
    data = []
    for e, e_batch in pData.items():
        x_axis = []
        y_axis = []
        z_axis = []

        for c, simulation in e_batch.items():
            for t, loss_percent_t in enumerate(simulation):
                x_axis.append(float(c.replace(",",".")))
                z_axis.append(loss_percent_t)
                y_axis.append(t)


        trace = go.Scatter3d(
            x=x_axis,
            y=y_axis,
            z=z_axis,
            name='e:'+e,
            mode='markers',
            marker=dict(
                size=3,
                opacity=0.8
            )
        )
        data.append(trace)

    layout = {'scene': {'yaxis': {'title': 'Round'}, 'xaxis': {'title': 'c'}, 'zaxis': {'title': 'loss_percent(Round)'}},
    'title': 'Loss percentage over time over c; using '+pAlgo+', '+pChurn}
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='Data/'+sys.argv[1]+'/3d_loss_percent_scatter.html')

if len(sys.argv) != 2:
    print("Usage: loss_percentage_over_c_time_e_scatter.py <timestamp of batch>")
    exit()

#Gets list of unique epsilons used in the simulation, as well as which algorithm and which churn was used
files = [i for i in os.listdir('Data/' + sys.argv[1] + "/") if i.endswith(".csv")]
epsilons = set()
algo = ""
churn = ""
for f in files:
    x = re.search(r"_e;.*_c",f)
    epsilons.add(f[x.span()[0]+3: x.span()[1]-2])
    x = re.search(r"algo;.",f)
    algo = (f[x.span()[0]: x.span()[1]])
    x = re.search(r"churn;.",f)
    churn = (f[x.span()[0]: x.span()[1]])
epsilons = list(epsilons)
epsilons.sort()


simulation = {}
#for each epsilon, opens file and appends loss percentage over c over rounds to simulation
for e in epsilons:
    simulation[e] = {}
    for f in files:
        if re.search(r"_e;"+e,f) == None:
            continue
        metadata, rounddata, tokendata = Analytics.csv_to_array('Data/' + sys.argv[1] + "/" + f)
        print(f)
        simulation[e][metadata[3]] = Analytics.loss_percent(tokendata)

#generates a graph: epsilon x c x time -> [0;1]
loss_percent_c_rounds_e(simulation, algo, churn)