import csv
class Analytics:
    """Reads csv to 2d arrys"""
    def csv_to_array(path):
        metadata, rounddata, tokendata = [], [], []
        with open(path, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=';', quotechar='|')
            metadata = (next(reader))
            for row in reader:
                tokendata.append(row[0:3])
                rounddata.append(row[-3:])
            return metadata, rounddata, tokendata

    """Returns the percentage of losttoken for each round"""
    def loss_percent(pTokendata):
        lossdata = []
        for row in pTokendata:
            lossdata.append(row[1])
        return lossdata

    def kill_percent(pRounddata):
        killdata = []
        for row in pRounddata:
            killdata.append(row[0])
        return killdata

    """Calculates the number of tokens in the network"""
    def sum_of_tokens(data):
        sums = []
        for row in data:
            sums.append(row[2])
        return sums

    """Returns the number of messages sent in each round.
    This information is in the last element of the rounddata"""
    def num_messages_per_round(data):
        messages = []
        for row in data:
            messages.append(row[-2])
        return messages
    
    def max_messages_per_round(data):
        messages = []
        for row in data:
            messages.append(row[-1])
        return messages

    """Calculates the backward finite difference. Padds the data at the beginning with zeros"""
    def finite_difference(data, spacing=1):
        difference = []
        data = [0]*spacing + data
        for i in range(1, len(data)-spacing+1):
            difference.append(data[i] - data[i-spacing])
        return difference