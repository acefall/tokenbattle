import sys
import datetime
from pprint import pprint
import plotly.offline as py
import plotly.graph_objs as go
import webbrowser, os
import glob

from analytics import Analytics

"""Generates scatter plot of f(rounds) and f'(rounds): f: rounds -> [0;1]"""
def generate_loss_graph(pMetadata, pRounddata, pTokendata):
    x_axis = list(range(1, int(pMetadata[1])+1))

    # Create traces
    trace_loss = go.Scatter(
        x = x_axis,
        y = Analytics.loss_percent(pTokendata),
        mode = 'markers',
        name = 'Tokens alive rate'
    )

    trace_kill = go.Scatter(
        x = x_axis,
        y = Analytics.kill_percent(pRounddata),
        mode = 'markers',
        name = 'Churn rate'
    )


    graphdata = [trace_loss, trace_kill]


    """layout = dict(title = 'n = '+pMetadata[0]
                  +'<br> rounds = '+str(len(pRounddata))
                  +'<br> epsilon = '+pMetadata[2]
                  +'<br> c = '+pMetadata[3]
                  +'<br> lambda = '+pMetadata[4]
                  +'<br> algorithm = '+pMetadata[5]
                  +'<br> churn = '+pMetadata[6])"""

    #fig = dict(data=graphdata, layout=layout)
    fig = dict(data=graphdata)
    py.plot(fig, filename=dt_str+'_loss.html', auto_open=False)

"""Generates scatter plot of total number of token copies in the network and messages that were sent in this round: rounds -> N x N"""
def generate_sum_of_tokens_graph(pMetadata, pRounddata, pTokendata):
    x_axis = list(range(1, int(pMetadata[1])+1))
    trace_sums = go.Scatter(
        x=x_axis,
        y=Analytics.sum_of_tokens(pTokendata),
        name='Total token copies in the network'
    )
    trace_messages = go.Scatter(
        x=x_axis,
        y=Analytics.num_messages_per_round(pRounddata),
        name='Total messages sent'
    )

    trace_max_messages = go.Scatter(
        x=x_axis,
        y=Analytics.max_messages_per_round(pRounddata),
        name='Maximum messages sent by a node'
    )

    graphdata = [trace_sums, trace_messages, trace_max_messages]
    layout = go.Layout(
        barmode='group'""",
        title = 'n = '+pMetadata[0]
                  +'<br> rounds = '+str(len(pRounddata))
                  +'<br> epsilon = '+pMetadata[2]
                  +'<br> c = '+pMetadata[3]
                  +'<br> lambda = '+pMetadata[4]
                  +'<br> algorithm = '+pMetadata[5]
                  +'<br> churn = '+pMetadata[6]"""
    )

    fig = go.Figure(data=graphdata, layout=layout)
    py.plot(fig, filename=dt_str+'_sum_tokens_num_messages.html', auto_open=False)
    

if not(len(sys.argv) == 2 or len(sys.argv) == 1):
    print("Usage: loss_percentage_num_messages_over_time.py <file to visualise>")
    exit()

tokendata = []
metadata  = []
rounddata  = []

# Datetime, so that the two files can be shown together on one page
dt = datetime.datetime.now()
dt_str = dt.strftime("%Y%m%d_%H%M%S")

if len(sys.argv) == 1:
	directorys = os.listdir("Data/")
	directorys.sort()
	print(directorys[-1])
	files = os.listdir("Data/"+directorys[-1])
	files.sort()
	file = files[-1]


#Read Data
if len(sys.argv) == 1:
	metadata, rounddata, tokendata = Analytics.csv_to_array("Data/"+directorys[-1]+"/"+file)
else:
	metadata, rounddata, tokendata = Analytics.csv_to_array("Data/"+sys.argv[1])

#Generate Graphs
generate_sum_of_tokens_graph(metadata, rounddata, tokendata)
generate_loss_graph(metadata, rounddata, tokendata)

#Build page that contains both graphs
frames = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <frameset rows="*, *">
	<frameset cols="50%,50%">
		<frame src='""" + dt_str + "_loss.html" + """'>
		<frame src='""" + dt_str + "_sum_tokens_num_messages.html" + """'>
	</frameset>
	
  </frameset>
</html>"""

#Open page
f = open(dt_str + "_frames.html", "w+")
f.write(frames)
webbrowser.open('file://' + os.path.realpath(dt_str + "_frames.html"))






