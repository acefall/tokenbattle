import csv
import sys
import datetime
from pprint import pprint
import plotly.offline as py
import plotly.graph_objs as go
import webbrowser, os
import numpy as np



"""Reads csv to 2d arrys"""
def csv_to_array(path):
    metadata, rounddata, tokendata = [], [], []
    with open(path, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='|')
        metadata = (next(reader))
        for row in reader:
            tokendata.append(row[:-2])
            rounddata.append(row[-2:])
        return metadata, rounddata, tokendata

"""computes the loss ratio in each round"""
def loss_ratio(pMetadata, pTokendata):
    lossratio = []
    for row in pTokendata:
        lossratio.append(row.count("0")/int(pMetadata[0]))
    return lossratio

"""Computes the argmax(i) of |T_i|/max(|T_j|)"""
def min_max_ratio(pTokendata):
    min_max_result_max = []
    min_max_result_min = []
    for row in pTokendata:
        row = [int(i) for i in row]
        t_max = max(row)
        min_max_result_max.append(max([row[i]/t_max for i in row]))
        min_max_result_min.append(min([row[i]/t_max for i in row]))
    return min_max_result_max, min_max_result_min


"""computes the loss ratio in each round, depending on c"""
def min_max_ratio_of_c(pBatch):
    return 0

"""3d-scatter-plot of loss ratio over time over c"""
def min_max_ratio_time_c(pData):
    x_axis = []
    y_axis = []
    z_axis1 = []
    z_axis2 = []

    for x in list(range(0, int(pData[0][0][1]))):
        for i in range(len(pData)):
            y = pData[i][0][3]
            x_axis.append(x+1)
            y_axis.append(y)
            z_axis1.append(pData[i][2][0][x])
            z_axis2.append(pData[i][2][1][x])

    #x, y, z = np.random.multivariate_normal(np.array([0,0,0]), np.eye(3), 200).transpose()
    trace1 = go.Scatter3d(
        x=x_axis,
        y=y_axis,
        z=z_axis1,
        mode='markers',
        marker=dict(
            size=3,
            line=dict(
                color='rgba(217, 217, 217, 0.14)',
                width=0.5
            ),
            opacity=0.8
        )
    )

    trace2 = go.Scatter3d(
        x=x_axis,
        y=y_axis,
        z=z_axis2,
        mode='markers',
        marker=dict(
            size=3,
            line=dict(
                color='rgba(255, 100, 100, 0.14)',
                width=0.5
            ),
            opacity=0.8
        )
    )

    data = [trace1, trace2]
    layout = {'scene': {'yaxis': {'type': 'log', 'title': 'c'}, 'xaxis': {'title': 'round'}, 'zaxis': {'title': '|T_j|/max(|T_i|)'}},
    'title': 'n = '+ pData[0][0][0]+', epsilon='+pData[0][0][2]}
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename=dt_str+'_3d_loss_ratio.html')

if len(sys.argv) != 2:
    print("Usage: 3d_loss_ratio.py <timestamp of batch>")
    exit()

dt = datetime.datetime.now()
dt_str = dt.strftime("%Y%m%d_%H%M%S")

metadata = []

files = [i for i in os.listdir('Data/') if i.startswith(sys.argv[1])]
c_data = []

for f in files:
    print(f)
    metadata, rounddata, tokendata = csv_to_array('Data/' + f)
    c_data.append([metadata, rounddata, min_max_ratio(tokendata)])


print(c_data)
min_max_ratio_time_c(c_data)



