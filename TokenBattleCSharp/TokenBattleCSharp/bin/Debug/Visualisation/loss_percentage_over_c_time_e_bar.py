import sys
import datetime
import plotly.offline as py
import plotly.graph_objs as go
import webbrowser, os
import numpy as np
import re

from analytics import Analytics

"""Generate Bar diagram: e x c -> rounds x [0;1]; Green inidcates 1 and red indicates 0"""
def loss_ratio_time_c(pData):
    x_axis = []
    percentiles = [.9, .8, .7, .6, .5, .4, .3, .2, .1, 0]
    traces = [[] for i in range(len(percentiles))]

    for episode in pData:
        x_axis.append(float(episode[0][3].replace(",",".")))
        r = 0
        totalRound = 0
        for p_index, percentile in enumerate(percentiles):
            while(r < len(episode[1]) and episode[1][r] >= percentile):
                r+=1
            traces[p_index].append(r-totalRound)
            totalRound = r
    data = []
    x = [1, 2, 3, 4]

    for p, percentile in enumerate(percentiles):
        data.append({
          'x': x_axis,
          'y': traces[p],
          'name': percentile,
          'type': 'bar',
          'marker': {'color':'rgb(' + str(round(255*(1-percentile))) + ', ' + str(round(255*percentile)) + ', 0)'}
        })
    
    layout = {
      'xaxis': {'title': 'c'},
      'yaxis': {'title': 'rounds'},
      'barmode': 'relative',
      'title': 'Rounds in which percentage of tokens were lost depending on c, epsilon = '+ pData[0][2]
    }
    py.plot({'data': data, 'layout': layout}, filename='Data/'+sys.argv[1]+'/'+e+'barmode-relative.html', auto_open=False)
    with open ('Data/'+sys.argv[1]+'/'+e+'barmode-relative.html', "a") as myfile:
        myfile.write("""</div>""")
        myfile.seek(0)
        myfile.write("""<div id='"""+str(float(pData[0][2].replace(",",".")))+"""'>""")

if len(sys.argv) != 2:
    print("Usage: loss_percentage_over_c_time_e_bar.py <timestamp of batch>")
    exit()

#Gets list of unique epsilons used in the simulation, as well as which algorithm and which churn was used
files = [i for i in os.listdir('Data/' + sys.argv[1] + "/") if i.endswith(".csv")]
epsilons = set()
algo = ""
churn = ""
for f in files:
    x = re.search(r"_e;.*_c",f)
    epsilons.add(f[x.span()[0]+3: x.span()[1]-2])
    x = re.search(r"algo;.",f)
    algo = (f[x.span()[0]: x.span()[1]])
    x = re.search(r"churn;.",f)
    churn = (f[x.span()[0]: x.span()[1]])
epsilons = list(epsilons)
epsilons.sort()

#For each epsilon, genrates a bar diagram over c and rounds that shows the percentage of lost tokens 
for e in epsilons:
    c_data = []
    metadata = []
    for f in files:
        if re.search(r"_e;"+e,f) == None:
            continue
        metadata, rounddata, tokendata = Analytics.csv_to_array('Data/' + sys.argv[1] + "/" + f)
        c_data.append([metadata, Analytics.loss_percent(tokendata), e])
    loss_ratio_time_c(c_data)

#Builds the overview file that contains all the different diagrams for the different epsilon
overview = """<!DOCTYPE html>
<html>
<title>Tokenbattle """+algo+churn+"""</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>
<div class="w3-container w3-top">
    <div class="w3-dropdown-hover">
      <button class="w3-button">Dropdown</button>
      <div class="w3-dropdown-content w3-bar-block w3-card-4">"""

for e in epsilons:
    overview += """<a href='#"""+str(float(e.replace(",",".")))+"""' class="w3-bar-item w3-button">"""+e+"""</a> """

overview +="""</div>
  </div>
</div>"""
for f in [i for i in os.listdir('Data/' + sys.argv[1] + "/") if i.endswith(".html")]:
    with open ('Data/' + sys.argv[1] + "/" +f, "r") as myfile:
        data=myfile.read()
    overview +=  data

overview += """</body>
</html>"""

#Opens overview
f = open('Data/' + sys.argv[1] + "/" + "overview.html", "w")
f.write(overview) 
webbrowser.open('file://' + os.path.realpath('Data/' + sys.argv[1] + "/" + "overview.html"))


