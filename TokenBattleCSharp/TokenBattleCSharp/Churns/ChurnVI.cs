﻿using System;
using System.Linq;

namespace TokenBattleCSharp.Churns
{
    internal class ChurnVI : IChurn
    {
        public double Churn(Graph _graph, double pEpsilon)
        {
            var n = 
                (((_graph.Strategy.Epsilon / 2) * -Math.Cos(2 * Math.PI * _graph.Round / _graph.Strategy.Lambda) +
                  (_graph.Strategy.Epsilon / 2)));

            ReplaceNodes(_graph, (int)Math.Round(n * _graph.Size));
            return n;
        }

        // Replaces random nodes
        public void ReplaceNodes(Graph _graph, float _toKill)
        {
            for (var k = 0; k < _toKill; k++)
            {
                _graph.Replace(_graph.Nodes[TokenBattle.Random.Next(_graph.Nodes.Count())]);
            }
        }
    }
}