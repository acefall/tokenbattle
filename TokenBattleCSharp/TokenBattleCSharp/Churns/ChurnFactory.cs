﻿namespace TokenBattleCSharp.Churns
{
    internal static class ChurnFactory
    {
        public static IChurn Get(int id)
        {
            switch (id)
            {
                case 0:
                    return new Churn0();
                case 1:
                    return new ChurnI();
                case 2:
                    return new ChurnII();
                case 3:
                    return new ChurnIII();
                case 4:
                    return new ChurnIV();
                case 5:
                    return new ChurnV();
                case 6:
                    return new ChurnVI();
                default:
                    throw new System.Exception("Churn not found.");
            }
        }
    }
}