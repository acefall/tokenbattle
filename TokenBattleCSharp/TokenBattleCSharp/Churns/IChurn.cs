﻿namespace TokenBattleCSharp.Churns
{
    public interface IChurn
    {
        double Churn(Graph _graph, double pEpsilon);
    }
}