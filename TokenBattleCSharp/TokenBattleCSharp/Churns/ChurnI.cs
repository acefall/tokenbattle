﻿using System;

namespace TokenBattleCSharp.Churns
{
    internal class ChurnI : IChurn
    {
        public double Churn(Graph _graph, double _epsilon)
        {
            var i = _graph.Strategy.Epsilon;

            ReplaceNodes(_graph, (int)Math.Round(i * _graph.Size));
            return i;
        }

        // Replaces oldest nodes first
        public void ReplaceNodes(Graph _graph, int _toKill)
        {
            var size = _graph.Nodes.Length;
            for (var k = 0; k < _toKill; k++)
            {
                _graph.Replace(_graph.Nodes[_graph.Oldest]);
                _graph.Oldest = (_graph.Oldest + 1) % size;
            }
        }
    }
}