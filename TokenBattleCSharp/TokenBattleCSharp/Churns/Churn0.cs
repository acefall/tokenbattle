﻿using System;

namespace TokenBattleCSharp.Churns
{
    internal class Churn0 : IChurn
    {
        public double Churn(Graph _graph, double _epsilon)
        {
            return 0;
        }
    }
}