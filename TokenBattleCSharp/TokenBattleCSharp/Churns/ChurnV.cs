﻿using System;
using System.Linq;

namespace TokenBattleCSharp.Churns
{
    internal class ChurnV : IChurn
    {
        public double Churn(Graph _graph, double pEpsilon)
        {
            var n = 
                (_graph.Strategy.Epsilon - _graph.Strategy.Epsilon / Math.Pow(1.006, _graph.Round));

            ReplaceNodes(_graph, (int)Math.Round(n * _graph.Size));
            return n;
        }

        // Replaces random nodes
        public void ReplaceNodes(Graph _graph, float _toKill)
        {
            for (var k = 0; k < _toKill; k++)
            {
                _graph.Replace(_graph.Nodes[TokenBattle.Random.Next(_graph.Nodes.Count())]);
            }
        }
    }
}